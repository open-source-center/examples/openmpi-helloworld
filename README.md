# EXAMPLE 
## Open MPI - Hello World

Simple "Hello World" example in C++ and utilizing Open MPI library.

### Compiling

```shell
cmake CMakeLists.txt
make
```

### Running

Run the following command

```shell
mpirun -n 2 ./openmpi-helloworld
```

### Result

```shell
Hello world from processor workrig, rank 1 out of 2 processors
Hello world from processor workrig, rank 0 out of 2 processors
```
